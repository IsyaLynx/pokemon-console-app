﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using PokeApiNet;
using Spectre.Console;

class Program
{
    static async Task Main(string[] args)
    {
        using var httpClient = new HttpClient();
        var pokeApiClient = new PokeApiClient(httpClient);

        //Console.Write("Enter pokemon name: "); 
        //string pokemonName = Console.ReadLine(); 

        if (args.Length == 0)
        {
            Console.WriteLine("Enter pokemon name: ");
            return;
        }

        string pokemonName = args[0];

        try
        {
            var pokemon = await pokeApiClient.GetResourceAsync<Pokemon>(pokemonName.ToLower());

            AnsiConsole.MarkupLine($"[green]name:[/] [pink1]{pokemon.Name}[/]");
            AnsiConsole.MarkupLine($"[green]id:[/] [pink1]{pokemon.Id}[/]");
            AnsiConsole.MarkupLine($"[green]type:[/] [pink1]{string.Join(", ", pokemon.Types.Select(t => t.Type.Name))}[/]");
            AnsiConsole.MarkupLine($"[green]height:[/] [pink1]{pokemon.Height}[/]");
            AnsiConsole.MarkupLine($"[green]default:[/] [pink1]{(pokemon.IsDefault ? "Yes" : "No")}[/]");
        }
        catch (Exception ex)
        {
            AnsiConsole.MarkupLine($"[red]Errors: {ex.Message}[/]");
        }
    }
}